/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16F1847
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "i2c_master.h"
#include "i2c_slave.h"
#include "csp/csp_if_i2c_uart.h"
#include "csp/csp_types.h"
#include "csp/network.h"
#include <stdio.h>
#include <string.h>

#define I2C_UART_FRAME_LEN sizeof(i2c_uart_frame_t)
static char i2c_in_buff[I2C_UART_FRAME_LEN];
static i2c_uart_frame_t *i2c_in_frame = (i2c_uart_frame_t *)i2c_in_buff;    
static csp_packet_t *i2c_in_packet = (csp_packet_t *)i2c_in_buff;
static i2c_uart_frame_t *i2c_in_buffer = (i2c_uart_frame_t *)i2c_in_buff;

static char uart_in_buff[I2C_UART_FRAME_LEN];
static i2c_uart_frame_t *uart_in_frame = (i2c_uart_frame_t *)uart_in_buff;    
static csp_packet_t *uart_in_packet = (csp_packet_t *)uart_in_buff;
static i2c_uart_frame_t *uart_in_buffer = (i2c_uart_frame_t *)uart_in_buff;

typedef enum state_n{
    ST_I2C_IN,
    ST_I2C_OUT,
    ST_UART_IN,
    ST_UART_OUT,
    ST_IDLE
}state_t;

static state_t curr_state;
static state_t next_state;

void blink(int n)
{
    for(int i=0; i<n; i++)
    {
        LED_Toggle();
        __delay_ms(50);
    }
}

void I2C_SlaveCustomRdInterruptHandler() {
    if(i2c_in_buffer->index < I2C_MTU)
    {
        uint8_t i2c_data = I2C_Read_Slave();
        i2c_in_buffer->data[i2c_in_buffer->index++] = i2c_data;
    }
    else
    {
        //Data not read, causes overflow, then NACK
        uint8_t i2c_data = I2C_Read_Slave();
        //i2c_in_buffer->index = 0;
    }
    
    if(SSP1STATbits.P)
    {
        // Received complete
        next_state = ST_UART_OUT;
//        i2c_in_frame->sync = I2C_UART_SYNC;
//        write_buff_uart(i2c_in_buff, I2C_UART_FRAME_LEN);
    }
    
}

void I2C_SlaveCustomWrInterruptHandler() {
    if(i2c_in_buffer->index < I2C_MTU)
        I2C_Write_Slave(i2c_in_buffer->data[i2c_in_buffer->index++]);
    else
        I2C_Write_Slave(0);
}

static void I2C_SlaveCustomAddrInterruptHandler() {
    uint8_t _ = I2C_Read_Slave(); // Clear the buffer
    next_state = ST_I2C_IN;
    memset(i2c_in_buffer, I2C_UART_FRAME_LEN, 0);
    i2c_in_buffer->index = 0; // Start writing or reading from 0
    LED_Toggle();
}

int read_buff_uart(char *buff, int len)
{
    int i = 0;
    memset(buff, 0, len);
    uint16_t *sync = (uint16_t *)buff;
   
    buff[0] = getch(); //putch(buff[0]);
    buff[1] = getch(); //putch(buff[1]);
    while(buff[0] != 'O' && buff[1] != 'K')
    {
        buff[0] = buff[1]; 
        buff[1] = getch(); //putch(buff[1]);
        
    }
    
    for(i=2; i<len; i++)
    {
        buff[i] = getch();  //putch(buff[i]);
    }
    return i;
}

int write_buff_uart(char *buff, int len)
{
    int i = 0;
    for(i=0; i<len; i++)
    {
        putch(buff[i]);
    }
    return i;
}

int csp_send_i2c(uint8_t address, char *buff, int len)
{
    while(!I2C_Open_Master(address)); // sit here until we get the bus..
    I2C_SetBuffer(buff, len);
    I2C_SetAddressNackCallback(NULL,NULL); //NACK polling?
    I2C_MasterWrite();
    while(I2C_BUSY == I2C_Close_Master()); // sit here until finished
    return 0;
}

void csp_resend(csp_packet_t *packet)
{
    packet->id.ext = ntohl(packet->id.ext);
    char dport = CSPID_GET_DPORT(packet->id);
    CSPID_SET_DPORT(packet->id, packet->id.sport);
    packet->id.sport = dport;
    char dst = CSPID_GET_DST(packet->id);
    CSPID_SET_DST(packet->id, packet->id.src);
    packet->id.src = dst;    
    packet->id.ext = htonl(packet->id.ext);
}


/*
                         Main application
 */
void main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    I2C_Initialize_Slave();


    // Enable Interrupts
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();

    I2C_Open_Slave();
    I2C_SlaveSetReadIntHandler(I2C_SlaveCustomRdInterruptHandler);
    I2C_SlaveSetWriteIntHandler(I2C_SlaveCustomWrInterruptHandler);
    I2C_SlaveSetAddrIntHandler(I2C_SlaveCustomAddrInterruptHandler);
    
    int rc;
//    blink(6);
    
    curr_state = ST_IDLE;
    next_state = ST_IDLE;

    while (1)
    {
        if(curr_state == ST_IDLE)
        {
            //blink(1);
            CLRWDT();
            if(EUSART_is_rx_ready())
                    next_state = ST_UART_IN;
            
        }
        else if(curr_state == ST_UART_IN)
        {
            memset(uart_in_buff, I2C_UART_FRAME_LEN, 0);
            rc = read_buff_uart(uart_in_buff, I2C_UART_FRAME_LEN);
            
            if(uart_in_frame->sync == htons(I2C_UART_SYNC))
                next_state = ST_I2C_OUT;
            else
                next_state = ST_IDLE;
        }
        else if(curr_state == ST_I2C_OUT)
        {
            // Disable Interrupts
            INTERRUPT_GlobalInterruptDisable();
            INTERRUPT_PeripheralInterruptDisable();
            // Reconfigure to master
            I2C_Close_Slave();
            I2C_Initialize_Master();
                    
            csp_send_i2c(uart_in_frame->addr, uart_in_frame->data, uart_in_frame->len_tx);

            //Reconfigure to slave
            I2C_Close_Master();
            I2C_Initialize_Slave();
            I2C_Open_Slave();
            I2C_SlaveSetReadIntHandler(I2C_SlaveCustomRdInterruptHandler);
            I2C_SlaveSetWriteIntHandler(I2C_SlaveCustomWrInterruptHandler);
            I2C_SlaveSetAddrIntHandler(I2C_SlaveCustomAddrInterruptHandler);
            //Enable interrupts
            INTERRUPT_GlobalInterruptEnable();
            INTERRUPT_PeripheralInterruptEnable();
            
            // Sync bytes
            puts("TX");

            //csp_resend(uart_in_packet);
            //write_buff_uart(uart_in_buff, I2C_UART_FRAME_LEN);
            
            next_state = ST_IDLE;

        }
        else if(curr_state == ST_I2C_IN)
        {
            //next_state = ST_I2C_IN;
        }
        else if(curr_state == ST_UART_OUT)
        {
//            blink(5);
            i2c_in_frame->sync = htons(I2C_UART_SYNC);
            i2c_in_frame->len = I2C_UART_FRAME_LEN;
            i2c_in_frame->len_tx = i2c_in_frame->index;
//            i2c_in_frame->addr = i2c_in_packet->id.src;
            write_buff_uart(i2c_in_buff, I2C_UART_FRAME_LEN);
            next_state = ST_IDLE;
        }

        curr_state = next_state;
    }
}
/**
 End of File
*/